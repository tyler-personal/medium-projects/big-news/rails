require 'test_helper'

class FrequencyFinderTest < ActiveSupport::TestCase
  test 'FrequencyFinder can determine word frequency' do
    titles = ['Potato Man', "What's the weather like today?", 'Oof', 'Weather', 'Oof', "What's the weather?"]
    result = FrequencyFinder.new.perform(titles)

    assert result.word_frequency['weather'] == 3
    assert result.word_frequency["what's"] == 2
  end

  test 'FrequencyFinder can determine phrase frequency' do
    titles = [
      'I like potato chips', 'They like potato chips',
      'I have a big button - Donald Trump', 'Donald Trump scared a baby', 'Donald Trump ate Donald Trump'
    ]
    result = FrequencyFinder.new.perform(titles)

    assert result.phrase_frequency['like potato chips'] == 2
    assert result.phrase_frequency['donald trump'] == 4
  end

  test "FrequencyFinder doesn't include a phrase that exists entirely in another phrase" do # TODO: Think about whether this is good or not
    result = FrequencyFinder.new.perform(['That man ran', 'That man jumped'])
    assert result.phrase_frequency['that man'] == 2

    result = FrequencyFinder.new.perform(['That man ran', 'That man ran fast'])
    assert_not result.phrase_frequency['that man']
    assert result.phrase_frequency['that man ran'] == 2
  end
end