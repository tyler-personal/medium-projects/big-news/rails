require 'test_helper'

class BBCTest < ActiveSupport::TestCase
  test 'BBC has proper values' do
    website = Websites::BBC.new(scraper: Mocks::WebScraper.new(url: Websites::BBC.url))

    string_array_attributes_test(website, %w[titles])
  end
end