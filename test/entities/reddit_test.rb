require 'test_helper'

class RedditTest < ActiveSupport::TestCase
  test 'Reddit has proper values' do
    website = Websites::Reddit.new(scraper: Mocks::WebScraper.new(url: Websites::Reddit.url))

    string_array_attributes_test(website, %w[titles comments])
  end
end