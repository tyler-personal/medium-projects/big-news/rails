require 'test_helper'

class GoogleNewsTest < ActiveSupport::TestCase
  test 'Google News has proper values' do
    website = Websites::GoogleNews.new(scraper: Mocks::WebScraper.new(url: Websites::GoogleNews.url))

    string_array_attributes_test(website, %w[titles])
  end
end