require 'test_helper'

class NPRTest < ActiveSupport::TestCase
  test 'NPR has proper values' do
    website = Websites::NPR.new(scraper: Mocks::WebScraper.new(url: Websites::NPR.url))

    string_array_attributes_test(website, %w[titles])
  end
end