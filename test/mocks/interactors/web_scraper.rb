module Mocks
  class WebScraper
    attr_accessor :url

    def initialize(url:)
      self.url = url
    end

    def perform(selector:, selector_type:)
      # TODO: Turn this into an assertion with an error message (stuck)
      return if selector.empty? || selector_type.empty? || url.empty?

      ['North Korea', 'Potatoes rule']
    end
  end
end

