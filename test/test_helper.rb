ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

require 'mocks/interactors/web_scraper.rb'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # TODO: Find a non-helper place to put this (or organize helper methods)
  def string_array_attributes_test(website, attrs)
    attrs.each do |attr|
      assert website.send(attr).count > 0, "#{attr} count not over 0"
      website.send(attr).each { |str| assert_instance_of String, str, "#{attr} contains a non-string" }
    end
  end
  # Add more helper methods to be used by all tests here...
end


