Rails.application.routes.draw do
  root 'main#landing_page'
  get 'main#home' => 'main#home', as: :main
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
