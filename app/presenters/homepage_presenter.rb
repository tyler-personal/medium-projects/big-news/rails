class HomepagePresenter
  def website_partials
    %w[reddit google_news bbc npr].map { |w| "main/home/#{w}" }
  end
end