class MainController < ApplicationController
  def home
    @presenter = HomepagePresenter.new
    titles = [Websites::Reddit, Websites::GoogleNews, Websites::NPR, Websites::BBC].map { |w| w.new.stories.map(&:titles) }.flatten
    result = FrequencyFinder.new.perform(titles)
    @frequent_stories = FrequencyFinder.new.perform(titles).phrase_frequency.sort_by { |_, v| v }.reverse
    @frequent_stories = result.word_frequency.sort_by { |_, v| v }.reverse
  end
end