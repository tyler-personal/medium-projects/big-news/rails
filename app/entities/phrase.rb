class Phrase
  include Comparable

  attr_reader :stories, :count

  def initialize(raw)
    self.raw = raw
    self.stories = []
  end

  def <=>(other)
    raw <=> other.to_s
  end

  def to_s
    raw
  end
  
  def add_stories

  end

  private

  attr_accessor :raw
  attr_writer :stories
end