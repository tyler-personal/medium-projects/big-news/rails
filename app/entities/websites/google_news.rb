module Websites
  class GoogleNews < Website
    def self.url
      'https://www.news.google.com'
    end

    def self.name
      'Google News'
    end

    def setup_attributes
      super(stories_selector: 'a[role=heading]')
    end
  end
end