module Websites
  class BBC < Website
    def self.url
      'http://www.bbc.com'
    end

    def self.name
      'British Broadcasting Corporation'
    end

    def setup_attributes
      super(stories_selector: 'a.media__link')
    end
  end
end