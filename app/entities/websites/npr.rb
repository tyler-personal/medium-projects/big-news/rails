module Websites
  class NPR < Website
    def self.url
      'https://www.npr.org'
    end

    def self.name
      'National Public Radio'
    end

    def setup_attributes
      super(stories_selector: '.title')
    end
  end
end