module Websites
  class Website
    attr_reader :url, :name, :stories, :word_frequency, :phrase_frequency, :popular_titles

    def initialize(scraper: WebScraper.new(url: self.class.url))
      @scraper = scraper
      setup_attributes
    end

    def setup_attributes(stories_selector:)
      @url = self.class.url
      @name = self.class.name

      @stories = @scraper.perform(selector: stories_selector).map { |r| Story.new(web_scraper_result: r) }
      result = FrequencyFinder.new.perform(@stories)

      @word_frequency = result.word_frequency
      @phrase_frequency = result.phrase_frequency
    end



  end
end