module Websites
  class Reddit < Website
    attr_reader :comments

    def self.url
      'https://www.reddit.com'
    end

    def self.name
      'Reddit'
    end

    def setup_attributes
      @comments = @scraper.perform(selector: 'a[data-event-action="comments"]')
      super(stories_selector: 'a.title')
    end

  end
end