class Story
  attr_reader :title, :url

  def initialize(title:, url:, web_scraper_result:)
    if web_scraper_result.present? # RANT: For the love of God give me constructor overloading
      @title = web_scraper_result.text
      @url = web_scraper_result.url
    else
      @title = title
      @url = url
    end
  end
end