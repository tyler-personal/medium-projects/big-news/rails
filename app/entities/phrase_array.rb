class PhraseArray < Array
  def << new_phrase
    if include? new_phrase
      phrase = self[index(new_phrase)]
      phrase.stories.push(new_phrase.story)
    end
  end
end