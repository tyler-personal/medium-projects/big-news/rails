require 'nokogiri'
require 'open-uri'

class WebScraper # TODO: UNIT TEST THIS
  def initialize(url:)
    # NOTE: The SSL verify_mode is for windows
    @website = Nokogiri::HTML(open(url, 'User-Agent' => 'Potato-Man', ssl_verify_mode: OpenSSL::SSL::VERIFY_NONE))
  end

  def perform(selector:, selector_type: :css)
    elements = @website.send(selector_type, selector)
    elements.map { |e| Results::WebScraper.new(text: e.text, url: e['href']) }
  end
end