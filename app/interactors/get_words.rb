class GetWords
  def perform(titles) # TODO: Get clever about checking for both lower && upcase, expose majority, instead of just lower
    titles.map { |t| t.downcase.split(/[^[[:word:]]']+/) }.flatten
  end
end