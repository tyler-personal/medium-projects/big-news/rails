class FrequencyFinder # TODO: UNIT TEST THIS TODO: FIX BUGS
  def perform(words) # TODO: Refactor out the type checking
    @words = words.instance_of?(Array) ? GetWords.new.perform(words) : words
    Result.new(find_word_frequency, find_phrase_frequency)
  end

  private

  def find_word_frequency
    @words.each_with_object(Hash.new(0)) { |word, h| h[word] += 1 }
  end

  def find_phrase_frequency
    phrase_frequency = Hash.new(0)

    2.upto(@words.length) do |phrase_length|
      @words.length.times do |first_word_index|
        phrase = @words[first_word_index, phrase_length]
        phrase_frequency[phrase] += 1 if phrase.length == phrase_length
      end
    end

    filter_phrases(phrase_frequency)
  end

  def filter_phrases(phrase_frequency)
    answer = phrase_frequency.reject { |words, count| count == 1 || words.reject(&:empty?).length == 1 }.map { |k, v| [k.join(' '), v] }.to_h

    answer.map do |k, _|
      answer.each do |k2, v2|
        if k2.include? k
          answer[k] -= v2 unless k2 == k
        end
      end
    end
    answer.reject { |_, v| v < 1 }
  end

  class Result
    attr_reader :word_frequency, :phrase_frequency

    def initialize(word_frequency, phrase_frequency)
      @word_frequency = word_frequency
      @phrase_frequency = phrase_frequency
    end

  end
end